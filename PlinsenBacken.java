/**
 *  Programm um Schuelern die Grundlagen der Programmierung zu erklaeren.
 *  
 *  @author Steffen Grahl
 *  @version 0.1
 */

public class PlinsenBacken {

    /**
     *  main method 
     *
     *  @param  args    command-line parameters
     */
    public static void main(String[] args) {
        if ( args.length == 0 ) {
            help();
        }
    }

    /**
     *  prints tips about using this class 
     *
     *
     */
    public static void help() {
        System.out.println("Bitte Parameter uebergeben");
    }
}