# Plinsen backen mit Java

This is not a working software. Please don't compile and run it on your machine.

I wrote this little piece of code to show young students and programming beginners how it is done. I chose an example from the real world which is easy and delicious. Feel free to use this code to educate others.

## Licence
Released under MIT Licence.
